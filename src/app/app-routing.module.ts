import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ZoomComponent } from './components/zoom/zoom.component';
import { AppComponent } from './app.component';

const routes: Routes = [
  {path: 'zoom', component: ZoomComponent},
  {path:' home', component: AppComponent},
  {path: '**', redirectTo: '/home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
