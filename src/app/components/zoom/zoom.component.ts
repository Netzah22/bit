import { Component} from '@angular/core';
import { ZoomMtg } from '@zoomus/websdk';

ZoomMtg.preLoadWasm();
ZoomMtg.prepareJssdk();

@Component({
  selector: 'app-zoom',
  templateUrl: './zoom.component.html',
  styleUrls: ['./zoom.component.css']
})
export class ZoomComponent{

  public meetConfig: any;
  public signature: any;

  constructor() {
    this.SetCongf(82633073108);
  }

  ngOnInit(): void {
  }

  SetCongf(val){
    this.meetConfig = {
      apiKey: '9dUehahjQWCT9dWQGpwx7Q',
      apiSecret: 'lKBkiacocDkuuR0cEBfW0ZYzDtwLVTiUOUOq',
      meetingNumber: val,
      userName: 'Netzah',
      passWord: 'DkZ6H3',
      leaveUrl: 'https://www.youtube.com/watch?v=Zu55qQCqgsI',
      role: 0
    };

    this.signature = ZoomMtg.generateSignature({
      meetingNumber: this.meetConfig.meetingNumber,
      apiKey: this.meetConfig.apiKey,
      apiSecret: this.meetConfig.apiSecret,
      role: this.meetConfig.role,
      success: res => {
        console.log(res.result);
      }
    });

    ZoomMtg.init({
      showMeetingHeader: false,
      disableInvite: true,
      disableCallOut: true,
      disableRecord: true,
      disableJoinAudio: true,
      audioPanelAlwaysOpen: true,
      showPureSharingContent: true,
      isSupportAV: true,
      isSupportChat: false,
      isSupportQA: false,
      isSupportCC: false,
      screenShare: true,
      rwcBackup: '',
      videoDrag: true,
      videoHeader: true,
      isLockBottom: false,
      isSupportNonverbal: false,
      isShowJoiningErrorDialog: false,
      leaveUrl: 'https://www.youtube.com/watch?v=Zu55qQCqgsI',
      success: res =>{
        ZoomMtg.join({
          meetingNumber: this.meetConfig.meetingNumber,
          userName: this.meetConfig.userName,
          signature: this.signature,
          apiKey: this.meetConfig.apiKey,
          userEmail: 'zoompruebaes@gmail.com',
          passWord: this.meetConfig.passWord,
          success: res => {
            console.log('Logueo correcto');
          },
          error: res => {
            console.log(res);
          }
        });
      },
      error: res => {
        console.log(res);
      }
    });
  }

}
